package com.example.mathquizsample;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

public class LevelListRecycler extends RecyclerView.Adapter<LevelListRecycler.ViewHolder> {
    // ArrayList of levels
    ArrayList<String> levelsList;
    Context context;
    private EventListener listener;

    // interface for taking actions when the list view is clicked
    public interface EventListener {
        void onClick(int position);
    }
    //Create public constructor for LevelListRecycler
    public LevelListRecycler(ArrayList<String> levelList, Context mContext) {
        levelsList = levelList;
        context = mContext;

    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Inflate cardView Layout
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate
                (R.layout.level_list_card_view, parent, false);
        return new ViewHolder(cv);
    }

    // Method for setting onClick Listener
    public void setListener(EventListener listener) {
        this.listener = listener;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, final int position) {
        CardView cv = holder.cardView;
        TextView levelText = cv.findViewById(R.id.level_text);
        RatingBar ratingBar = cv.findViewById(R.id.rating_bar);
        TextView difficulty = cv.findViewById(R.id.difficulty);
        String currentLevel = levelsList.get(position);

        if (position == 0) {
            levelText.setText(currentLevel);
            difficulty.setText("Easy");
            ratingBar.setRating(1);
        } else if (position == 1) {
            levelText.setText(currentLevel);
            ratingBar.setRating(3);
            difficulty.setText("Medium");
        } else if (position == 2) {
            levelText.setText(currentLevel);
            ratingBar.setRating(5);
            difficulty.setText("Hard");
        }

        cv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (listener != null) {
                    listener.onClick(position);
                }


            }
        });
    }

    @Override
    public int getItemCount() {
        return levelsList.size();
    }



    // Create ViewHolder Class for cardView
    public class ViewHolder extends RecyclerView.ViewHolder {
        private CardView cardView;

        // ViewHolder constructor
        public ViewHolder(CardView cardView1) {
            super(cardView1);
            cardView = cardView1;

        }
    }


}
