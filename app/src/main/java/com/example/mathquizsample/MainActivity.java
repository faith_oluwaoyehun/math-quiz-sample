package com.example.mathquizsample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.LineNumberReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    //Shared preference for storing total points
    SharedPreferences totalPoints;
    //TextView for totalPoints
    TextView totalPointsTextView;
    //List views Declarations
    private RecyclerView recyclerView;
    private LinearLayoutManager linearLayoutManager;
    private LevelListRecycler levelListRecycler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        //create sharedPreferences for total points;
        totalPoints = this.getSharedPreferences("Total Points", MODE_PRIVATE);
        SharedPreferences.Editor editor = totalPoints.edit();
        editor.putInt("Points", 0);
        editor.apply();

        //Bind All views
        recyclerView = findViewById(R.id.levelList_recyclerView);
        totalPointsTextView = findViewById(R.id.cummulative_points);

    }

    @Override
    protected void onStart() {
        Launch();
        upDateTotalPoints();
        super.onStart();
    }

    @Override
    protected void onResume() {
        upDateTotalPoints();
        super.onResume();
    }
    /*
    Mechanism of getting quiz questions from the arrayLists and storing
     them in the database on first app launch*/

    public void addQuestionsToDatabase() {
        QuestionsAndAnswers questionsAndAnswers = new QuestionsAndAnswers();

        // Get Number of questions
        int noOfQuestions = questionsAndAnswers.getQuestionsSize();

        //Iterator int
        int iterator = 0;

        // Create QuizDBHelper Object
        QuizDatabaseHelper databaseHelper = new QuizDatabaseHelper(this, "QuizDatabase",
                null, 1);

        //Store questions in a database only on first launch
        while (iterator < noOfQuestions) {
            QuestionsClass questionsClass = new QuestionsClass();
            questionsClass.setQuestion(questionsAndAnswers.getQuestions(iterator));
            questionsClass.setOption_A(questionsAndAnswers.getOption_A(iterator));
            questionsClass.setOption_B(questionsAndAnswers.getOption_B(iterator));
            questionsClass.setOption_C(questionsAndAnswers.getOption_C(iterator));
            questionsClass.setOptiom_D(questionsAndAnswers.getOption_D(iterator));
            questionsClass.setAnswer(questionsAndAnswers.getAnswer(iterator));
            questionsClass.setDifficulty(questionsAndAnswers.getDifficulty(iterator));
            //Insert into database
            databaseHelper.insertValuesIntoDatabase(questionsClass);
            iterator++;

        }


    }

    //Method for loading and displaying the list of levels available in the quiz app.
    public void ListOfLevels() {
        ArrayList<String> levelList = new ArrayList<>();
        levelList.add("Level 1");
        levelList.add("Level 2");
        levelList.add("Level 3");
        linearLayoutManager = new LinearLayoutManager(this);
        levelListRecycler = new LevelListRecycler(levelList, this);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setAdapter(levelListRecycler);

        // Create a listener for click events on cards
        levelListRecycler.setListener(new LevelListRecycler.EventListener() {

            @Override
            public void onClick(int position) {
                if (position == 0) {
                    LoadRules(position);
                } else if (position == 1) {
                    LoadRules(position);
                } else if (position == 2) {
                    LoadRules(position);
                }
            }
        });

    }

    //Method for updating total points
    public void upDateTotalPoints() {
        SharedPreferences updateTotalPoints = PreferenceManager.getDefaultSharedPreferences(this);
        int pointsFromPreferences = updateTotalPoints.getInt("Points", 0);
        totalPointsTextView.setText("Total Points: " + pointsFromPreferences);

    }

    /**
     * Method for first app launch after install.
     * // This method checks whether the app is being launched for the first time. If so, it loads the questions into the database
     **/
    public void Launch() {
        SharedPreferences savedPreference = this.getSharedPreferences("FIRST_LAUNCH", MODE_PRIVATE);
        int saveVersionCode = savedPreference.getInt("VersionCode", -1);
        /** Get version code of the app. Version code increases when app is updated**/
        int currentVersionCode = BuildConfig.VERSION_CODE;
        SharedPreferences sharedPreferences = this.getSharedPreferences("FIRST_LAUNCH", MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();

        /**If version code in sharedPreferences is not equal to current version code of the app, then things are done as
         shown in the if/else statements below**/
        //For first ever launch
        if (saveVersionCode == -1) {
            AddQuestionsInBackground addQuestionsInBackground = new AddQuestionsInBackground();
            addQuestionsInBackground.execute();
            editor.putInt("VersionCode", currentVersionCode);
            editor.apply();
            return;

            //For updated app
        } else if (saveVersionCode < currentVersionCode) {
            AddQuestionsInBackground addQuestionsInBackground = new AddQuestionsInBackground();
            addQuestionsInBackground.execute();
            editor.putInt("VersionCode", currentVersionCode);
            editor.apply();

        } else {
            //Database is already loaded, initialize the list of levels and show them to the user.
            ListOfLevels();
        }


    }

    //Method for loading rules dialog
    public void LoadRules(final int position) {
        final Dialog dialog = new Dialog(this);

        //Set up views in the dialog
        dialog.setContentView(R.layout.custom_alert_dialog_for_rules_level);
        TextView heading = dialog.findViewById(R.id.rules_heading);
        Button okButton = dialog.findViewById(R.id.rules_understood_button);
        ListView listView = dialog.findViewById(R.id.list_of_rules);
        List<String> rules = new ArrayList<>();
        ArrayAdapter<String> rulesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, rules);
        if (position == 0) {
            heading.setText("RULES FOR LEVEL 1");
            String[] rulesString = getResources().getStringArray(R.array.Level_1_Rules);
            rules = Arrays.asList(rulesString);
            rulesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, rules);
        } else if (position == 1) {
            heading.setText("RULES FOR LEVEL 2");
            String[] rulesString = getResources().getStringArray(R.array.Level_2_Rules);
            rules = Arrays.asList(rulesString);
            rulesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, rules);
        } else if (position == 2) {
            heading.setText("RULES FOR LEVEL 3");
            String[] rulesString = getResources().getStringArray(R.array.Level_3_Rules);
            rules = Arrays.asList(rulesString);
            rulesAdapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, rules);
        }
        listView.setAdapter(rulesAdapter);
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.cancel();
                Intent quizActivity = new Intent(v.getContext(), QuizActivity.class);
                quizActivity.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                quizActivity.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                //Send quiz requirements based on the level clicked in the list
                if (position == 0) {
                    quizActivity.putExtra("numberOfQuestions", 20);
                    quizActivity.putExtra("Difficulty", 1);
                    quizActivity.putExtra("Time", 180000);
                    finish();
                } else if (position == 1) {
                    quizActivity.putExtra("numberOfQuestions", 30);
                    quizActivity.putExtra("Difficulty", 2);
                    quizActivity.putExtra("Time", 270000);
                    finish();
                } else if (position == 2) {
                    quizActivity.putExtra("numberOfQuestions", 40);
                    quizActivity.putExtra("Difficulty", 3);
                    quizActivity.putExtra("Time", 360000);
                    finish();
                }
                v.getContext().startActivity(quizActivity);


            }
        });
        dialog.show();
    }

    // AysncTask for loading questions into database on first launch
    private class AddQuestionsInBackground extends AsyncTask<Void, Void, Void> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            // Show a progress dialog while database in being set up in background
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Wait a few seconds as database is being set up. This will only occur once");
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... voids) {
            addQuestionsToDatabase();
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            progressDialog.dismiss();
            ListOfLevels();
            super.onPostExecute(aVoid);
        }
    }


    private class QuestionsAndAnswers {

        // Questions and Answer class
        public ArrayList<String> questions;
        public ArrayList<String> option_A;
        public ArrayList<String> option_B;
        public ArrayList<String> option_C;
        public ArrayList<String> option_D;
        public ArrayList<String> answer;
        public ArrayList<Integer> difficulty;

        // These are used to read questions into the database from the assets folder where questions and answers are contained
        AssetManager assetManager;
        InputStream inputStream;
        LineNumberReader lineNumberReader;
        String line;


        public QuestionsAndAnswers() {
            storeQuestions();
            storeAnswers();
            storeDifficulty();
            storeOptionA();
            storeOptionB();
            storeOptionC();
            storeOptionD();

        }


        //Method for storing Questions
        public void storeQuestions() {
            questions = new ArrayList<>();
            try {
                assetManager = getAssets();
                inputStream = assetManager.open("Questions.txt");
                lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream));
                while ((line = lineNumberReader.readLine()) != null) {
                    questions.add(line);
                }
            } catch (IOException e) {
                Log.e("QUIZ", "Error reading file", e);

            }

        }

        //Method for storing Option As
        public void storeOptionA() {
            option_A = new ArrayList<>();
            try {
                assetManager = getAssets();
                inputStream = assetManager.open("Option_A.txt");
                lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream));
                while ((line = lineNumberReader.readLine()) != null) {
                    option_A.add(line);
                }
            } catch (IOException e) {
                Log.e("QUIZ", "Error reading file", e);

            }

        }

        //Method for storing option Bs
        public void storeOptionB() {
            option_B = new ArrayList<>();
            try {
                assetManager = getAssets();
                inputStream = assetManager.open("Option_B.txt");
                lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream));
                while ((line = lineNumberReader.readLine()) != null) {
                    option_B.add(line);
                }
            } catch (IOException e) {
                Log.e("QUIZ", "Error reading file", e);

            }

        }

        //Method for storing option Cs
        public void storeOptionC() {
            option_C = new ArrayList<>();
            try {
                assetManager = getAssets();
                inputStream = assetManager.open("Option_C.txt");
                lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream));
                while ((line = lineNumberReader.readLine()) != null) {
                    option_C.add(line);
                }
            } catch (IOException e) {
                Log.e("QUIZ", "Error reading file", e);

            }

        }

        //Method for storing option Ds
        public void storeOptionD() {
            option_D = new ArrayList<>();
            try {
                assetManager = getAssets();
                inputStream = assetManager.open("Option_d.txt");
                lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream));
                while ((line = lineNumberReader.readLine()) != null) {
                    option_D.add(line);
                }
            } catch (IOException e) {
                Log.e("QUIZ", "Error reading file", e);

            }
        }

        //Method for storing answers
        public void storeAnswers() {
            answer = new ArrayList<>();
            try {
                assetManager = getAssets();
                inputStream = assetManager.open("Answers.txt");
                lineNumberReader = new LineNumberReader(new InputStreamReader(inputStream));
                while ((line = lineNumberReader.readLine()) != null) {
                    answer.add(line);
                }
            } catch (IOException e) {
                Log.e("QUIZ", "Error reading file", e);

            }

        }

        //Method for storing difficulty
        public void storeDifficulty() {
            difficulty = new ArrayList<>();
            int diff = 1;
            for (int i = 1; i < 338; ) {
                if (i > 100 && i < 213) {
                    diff = 2;
                }
                if (i > 212) {
                    diff = 3;
                }
                difficulty.add(diff);
                i++;
            }

        }

        //Getter methods
        public String getOption_A(int position) {
            return option_A.get(position);
        }

        public String getOption_B(int position) {
            return option_B.get(position);
        }

        public String getQuestions(int position) {
            return questions.get(position);
        }

        public String getAnswer(int position) {
            return answer.get(position);
        }

        public int getDifficulty(int position) {
            return difficulty.get(position);
        }

        public String getOption_C(int position) {
            return option_C.get(position);
        }

        public String getOption_D(int position) {
            return option_D.get(position);
        }

        //Method to get number of questions in the array list
        public int getQuestionsSize() {
            return answer.size();
        }
    }

}
