package com.example.mathquizsample;

public class QuestionsClass {

    //Create Variables
    private String question;
    private String option_A;
    private String option_B;
    private String option_C;
    private String optiom_D;
    private String answer;
    private int difficulty;

    //Create public constructor for QuestionClass
    public QuestionsClass(String mQuestion, String mOption_A,
                          String mOption_B, String mOption_C, String mOption_D,
                          String mAnswer, int mDifficulty){
        question = mQuestion;
        option_A = mOption_A;
        option_B = mOption_B;
        option_C = mOption_C;
        optiom_D = mOption_D;
        answer = mAnswer;
        difficulty = mDifficulty;
    }

    public QuestionsClass(){

    }

    //All public getter methods

    public String getOption_A() {
        return option_A;
    }

    public String getQuestion() {
        return question;
    }

    public String getOption_B() {
        return option_B;
    }

    public String getAnswer() {
        return answer;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public String getOptiom_D() {
        return optiom_D;
    }

    public String getOption_C() {
        return option_C;
    }

    //All setter methods

    public void setOption_A(String option_A) {
        this.option_A = option_A;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public void setOption_B(String option_B) {
        this.option_B = option_B;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public void setOptiom_D(String optiom_D) {
        this.optiom_D = optiom_D;
    }

    public void setOption_C(String option_C) {
        this.option_C = option_C;
    }
}
