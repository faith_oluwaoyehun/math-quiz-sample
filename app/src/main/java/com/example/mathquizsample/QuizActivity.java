package com.example.mathquizsample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.view.View;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.Random;
import java.util.Set;

public class QuizActivity extends AppCompatActivity {

    // Time question,difficulty, score, questionCount textView
    TextView countdownTimer, question, difficultyTextView, scoreText, questionCount;

    RadioGroup radioGroup;

    //Option radioButtons
    RadioButton optionA, optionB, optionC, optionD;

    //ArrayList where questions are taken from
    ArrayList<QuestionsClass> questionBank;

    //Answer string
    String UserAnswer;

    //Question class for each question
    QuestionsClass questionsClass;

    //Boolean for checking if random numbers have been generated for taking questions from questionBank
    boolean questionsToAskReady = false;

    //Score, questionNumber, randomQuestionNumber;
    int presentScore = 0;
    int questionNumbering = 1;
    int presentRandomNumber = 0;

    //Constant for determining if Quiz Activity have been launched before to prevent reloading of the questions after onPause and onStart
    boolean ALREADY_LOADED = false;


    //Random numbers arrayList
    Integer[] randomNumbers;

    //Difficulty, Number of Questions, Time
    int difficulty;
    int noOfQuestions;
    int timeInMilliseconds;

    //AlertDialog for leaving quiz page abruptly
    AlertDialog alertDialog;

    //CountdownTimer for quiz
    CountDownTimer countTimer;

    //boolean for checking if time was up or user completed questions
    boolean TIME_UP = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);


        //Register broadcast for time up
        IntentFilter timeIntentFilter = new IntentFilter("TIME UP");
        LocalBroadcastManager.getInstance(this).registerReceiver(timeCountFinishedReceiver, timeIntentFilter);


        // Bind all views in the layout
        countdownTimer = findViewById(R.id.time_textView);
        question = findViewById(R.id.question_text);
        difficultyTextView = findViewById(R.id.difficlty_textView);
        questionCount = findViewById(R.id.questionCount);
        scoreText = findViewById(R.id.scoreText);
        optionA = findViewById(R.id.optionA);
        optionB = findViewById(R.id.optionB);
        optionC = findViewById(R.id.optionC);
        optionD = findViewById(R.id.optionD);
        radioGroup = findViewById(R.id.radio_group);

        checkQuizParamaters();


    }

    @Override
    protected void onStart() {
        // Load questions from background thread
        if (!ALREADY_LOADED) {
            LoadingQuestionsInBackground loadingQuestionsInBackground = new LoadingQuestionsInBackground();
            loadingQuestionsInBackground.execute();

        }
        super.onStart();
    }

    //Method for checking quiz parameters received from intent
    public void checkQuizParamaters() {
        Intent quizParameters = getIntent();
        difficulty = quizParameters.getIntExtra("Difficulty", 1);
        noOfQuestions = quizParameters.getIntExtra("numberOfQuestions", 0);
        timeInMilliseconds = quizParameters.getIntExtra("Time", 0);

        difficultyTextView.setText("Level: " + difficulty);

    }

    //Method for timer countdown
    public void Timer() {
        countTimer = new CountDownTimer(timeInMilliseconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                countdownTimer.setText(String.valueOf(millisUntilFinished / 1000));
            }

            @Override
            public void onFinish() {
                TIME_UP = true;
                Intent timeIntent = new Intent("TIME UP");
                LocalBroadcastManager.getInstance(QuizActivity.this).sendBroadcast(timeIntent);
            }
        };
        countTimer.start();
    }

    //Method for loading question and answers into arrayList
    public void LoadQuestionsAndAnswers() {
        QuizDatabaseHelper quizDatabaseHelper = new QuizDatabaseHelper(this, "QuizDatabase",
                null, 1);
        questionBank = new ArrayList<>();
        questionBank = quizDatabaseHelper.getQuestions(difficulty);
        randomNumbers = QuestionToAsk();

    }

    //Method for displaying questions and answers from arrayList
    public void displayQA() {
        //Check if questions have been completed first

        if (questionNumbering > noOfQuestions) {
            Completed();
            return;
        } else {

            // questionsToAskReady used to check if the randomNumbers are ready for use or not.If false, get numbers first
            if (questionsToAskReady == false) {
                randomNumbers = QuestionToAsk();
            }

            int questionNumber = randomNumbers[presentRandomNumber];


            if (questionBank != null) {
                questionsClass = questionBank.get(questionNumber);
                question.setText(questionsClass.getQuestion());
                optionA.setText(questionsClass.getOption_A());
                optionB.setText(questionsClass.getOption_B());
                optionC.setText(questionsClass.getOption_C());
                optionD.setText(questionsClass.getOptiom_D());

                questionCount.setText("Question " + questionNumbering + " out of " + noOfQuestions);
                scoreText.setText(String.valueOf(presentScore + " points"));
                presentRandomNumber++;

            } else {
                Toast.makeText(this, "No questions in database", Toast.LENGTH_LONG).show();
            }
        }

    }

    //Method for getting randomNumbers
    public Integer[] QuestionToAsk() {

        Random r = new Random();

        Integer randomNumberBound = Integer.valueOf(noOfQuestions);

        if (difficulty == 1) {
            randomNumberBound = 100;
        } else if (difficulty == 2) {
            randomNumberBound = 112;
        } else if (difficulty == 3) {
            randomNumberBound = 125;
        }
        Set<Integer> set = new LinkedHashSet<Integer>();
        while (set.size() < noOfQuestions) {
            set.add(r.nextInt(randomNumberBound));
        }
        Integer[] numbersBank = new Integer[set.size()];
        set.toArray(numbersBank);
        questionsToAskReady = true;
        return numbersBank;

    }

    //Method for assigning user answer to options
    public String GetUSerAnswer() {
        if (optionA.isChecked()) {
            UserAnswer = "A";
        } else if (optionB.isChecked()) {
            UserAnswer = "B";
        } else if (optionC.isChecked()) {
            UserAnswer = "C";
        } else if (optionD.isChecked()) {
            UserAnswer = "D";
        } else {
            UserAnswer = "E";
        }
        return UserAnswer;
    }

    //Method for scoring and updating score
    public void score(String userAnswer, boolean skipOrNot) {
        if (userAnswer.equals(questionsClass.getAnswer())) {
            if (!skipOrNot) {
                presentScore = presentScore + 3;
            }
        }
        if (!userAnswer.equals(questionsClass.getAnswer())) {
            if (!skipOrNot) {
                presentScore = presentScore - 1;
            }
        }
        radioGroup.clearCheck();
        questionNumbering = questionNumbering + 1;
    }

    //Method for when the questions are completed
    public void Completed() {
        if (TIME_UP) {
            countTimer.cancel();
            final Dialog dialog = new Dialog(this);
            dialog.setContentView(R.layout.time_up);
            dialog.setCancelable(false);
            dialog.show();
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    dialog.cancel();
                    Intent sendToResultActivity = new Intent(QuizActivity.this, ResultActivity.class);
                    sendToResultActivity.putExtra("Score", presentScore);
                    sendToResultActivity.putExtra("Difficulty", difficulty);
                    sendToResultActivity.putExtra("NoOfQuestions", noOfQuestions);
                    sendToResultActivity.putExtra("Time", timeInMilliseconds);
                    startActivity(sendToResultActivity);
                    finish();
                }
            }, 1000);
        } else {
            countTimer.cancel();
            Intent sendToResultActivity = new Intent(QuizActivity.this, ResultActivity.class);
            sendToResultActivity.putExtra("Score", presentScore);
            sendToResultActivity.putExtra("Difficulty", difficulty);
            sendToResultActivity.putExtra("NoOfQuestions", noOfQuestions);
            sendToResultActivity.putExtra("Time", timeInMilliseconds);
            startActivity(sendToResultActivity);
            finish();
        }

    }


    //Method for next question
    public void NextQuestion(View v) {
        score(GetUSerAnswer(), false);

        displayQA();

    }

    //Method for skip question
    public void SkipQuestion(View view) {
        score(GetUSerAnswer(), true);

        displayQA();


    }

    //Broadcast receiver for time up and questions finished. I am using the same intent for both.
    private BroadcastReceiver timeCountFinishedReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            if (intent.getAction().equals("TIME UP")) {
                Completed();

            }
        }
    };


    // AysncTask for loading questions in Background

    private class LoadingQuestionsInBackground extends AsyncTask<Void, Integer, String> {
        ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {

            progressDialog = new ProgressDialog(QuizActivity.this);
            progressDialog.setTitle("Loading Quiz");
            progressDialog.setMessage("Please wait a few seconds");
            progressDialog.setCancelable(false);
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(Void... voids) {
            LoadQuestionsAndAnswers();


            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            progressDialog.dismiss();
            displayQA();
            Timer();
            ALREADY_LOADED = true;


            super.onPostExecute(s);
        }
    }

    @Override
    protected void onDestroy() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(timeCountFinishedReceiver);
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this)
                .setMessage("Submit Quiz?")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setCancelable(true)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        alertDialog.dismiss();
                        Completed();

                    }
                });
        alertDialog = builder.create();
        alertDialog.show();

    }

    @Override
    protected void onPause() {
        if (alertDialog != null) {
            alertDialog.dismiss();
        }
        super.onPause();
    }
}
