package com.example.mathquizsample;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

public class QuizDatabaseHelper extends SQLiteOpenHelper {

    //Table NAme
    private final String TABLE_NAME = "QUIZ_DATABASE";
    private final String TOTAL_SCORE = "SCORE";

    //Public constructor for QuizDatabaseHelper
    public QuizDatabaseHelper(Context context, String DBNAme, SQLiteDatabase.CursorFactory factory,
                              int Version) {
        super(context, "QuizDatabase", null, 1);
    }


    @Override
    public void onCreate(SQLiteDatabase db) {

        //Create QuizTable
        db.execSQL("CREATE TABLE  " + TABLE_NAME + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "QUESTION TEXT," +
                "OPTION_A TEXT," +
                "OPTION_B TEXT," +
                "OPTION_C TEXT," +
                "OPTION_D TEXT," +
                "ANSWER TEXT," +
                "DIFFICULTY INTEGER )");
        db.execSQL("CREATE TABLE  " + TOTAL_SCORE + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "SCORE INTEGER)");


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {


    }

    //Insert rows of questions into the database
    public void insertValuesIntoDatabase(QuestionsClass questionsClass) {
        ContentValues contentValues = new ContentValues();
        contentValues.put("QUESTION", questionsClass.getQuestion());
        contentValues.put("OPTION_A", questionsClass.getOption_A());
        contentValues.put("OPTION_B", questionsClass.getOption_B());
        contentValues.put("OPTION_C", questionsClass.getOption_C());
        contentValues.put("OPTION_D", questionsClass.getOptiom_D());
        contentValues.put("ANSWER", questionsClass.getAnswer());
        contentValues.put("DIFFICULTY", questionsClass.getDifficulty());

        SQLiteDatabase database = getWritableDatabase();
        database.insert(TABLE_NAME, null, contentValues);

    }

    //Method for getting questions for a particular difficulty
    public ArrayList<QuestionsClass> getQuestions(int difficulty) {
        //This is the arraylist that will be returned to the quiz activity

        ArrayList<QuestionsClass> questionsBank = new ArrayList<>();
        SQLiteDatabase liteDatabase = getReadableDatabase();
        Cursor cursor = liteDatabase.rawQuery("SELECT * FROM " + TABLE_NAME + " WHERE " + " DIFFICULTY " + " =\"" + difficulty + "\"", null);
        cursor.getCount();

        int i = 0;

        // Loop through the cursor to get questions and add them to the arraylist
        while (i < cursor.getCount()) {
            QuestionsClass questionsClass = new QuestionsClass();
            if (i == 0) {
                cursor.moveToFirst();
            }
            questionsClass.setQuestion(cursor.getString(1));
            questionsClass.setOption_A(cursor.getString(2));
            questionsClass.setOption_B(cursor.getString(3));
            questionsClass.setOption_C(cursor.getString(4));
            questionsClass.setOptiom_D(cursor.getString(5));
            questionsClass.setAnswer(cursor.getString(6));
            questionsClass.setDifficulty(cursor.getInt(7));
            questionsBank.add(questionsClass);
            cursor.moveToNext();
            i++;
        }

        cursor.close();
        return questionsBank;


    }

}
