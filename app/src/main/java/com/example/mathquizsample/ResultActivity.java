package com.example.mathquizsample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.lang.reflect.Method;

public class ResultActivity extends AppCompatActivity {

    //TextViews
    TextView scoreTextView, result, totalPoints;

    //Buttons
    Button takeAnotherQuiz, returnToMainMenu;

    //result, time, difficulty, number of questions, formerTotalPoints,
    int score;
    int time;
    int difficulty;
    int NoofQuestions;
    int formerTotalPoints;

    //Shared preferences for result
    SharedPreferences sharedPreferences;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        //Bind All Views;
        scoreTextView = findViewById(R.id.scoreView);
        result = findViewById(R.id.result);
        totalPoints = findViewById(R.id.total_points);
        takeAnotherQuiz = findViewById(R.id.take_another);
        returnToMainMenu = findViewById(R.id.return_mainMenu);


        getQuizParameters();
        result.setText(score + " points");

    }

    @Override
    protected void onStart() {

        SharedPreferences updateTotalPoints = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        int pointsFromPreferences = updateTotalPoints.getInt("Points", 0);
        SharedPreferences.Editor editor = updateTotalPoints.edit();
        editor.putInt("Points", score + pointsFromPreferences);
        editor.apply();
        super.onStart();
    }


    //Method for taking another quiz
    public void takeAnother(View view) {
        Intent anotherQuiz = new Intent(this, QuizActivity.class);
        anotherQuiz.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        anotherQuiz.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        anotherQuiz.putExtra("Difficulty", difficulty);
        anotherQuiz.putExtra("numberOfQuestions", NoofQuestions);
        anotherQuiz.putExtra("Time", time);
        startActivity(anotherQuiz);
        finish();
    }

    //Method for returning to main menu
    public void returnToMainMenu(View view) {
        Intent mainMenu = new Intent(this, MainActivity.class);
        startActivity(mainMenu);
        finish();


    }

    /**
     * Method for getting all quiz parameters inCase the user wants to take another
     * quiz directly without going back first to the mainActivity
     */
    public void getQuizParameters() {
        //Get score, difficulty, time  from quiz activity
        Intent intent = getIntent();
        score = intent.getIntExtra("Score", 0);
        time = intent.getIntExtra("Time", 0);
        NoofQuestions = intent.getIntExtra("NoOfQuestions", 0);
        difficulty = intent.getIntExtra("Difficulty", 1);


    }

    @Override
    public void onBackPressed() {
        Intent mainMenu = new Intent(this, MainActivity.class);
        startActivity(mainMenu);
        finish();
        super.onBackPressed();
    }
}

